package com.adelingo.android.swapi.ui.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adelingo.android.swapi.R;

// Диалог для отображения информации
public class InfoDialog extends DialogFragment {
    public static final String ARGUMENT_TAG_TITLE = "title";
    public static final String ARGUMENT_TAG_CONTENT = "content";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        String title = bundle.getString(ARGUMENT_TAG_TITLE, "null");
        String content = bundle.getString(ARGUMENT_TAG_CONTENT, "null");
        View view = inflater.inflate(R.layout.dialog_info, null);
        TextView textView = (TextView) view.findViewById(R.id.textView);
        textView.setText(content);
        getDialog().setTitle(title);
        return view;
    }
}

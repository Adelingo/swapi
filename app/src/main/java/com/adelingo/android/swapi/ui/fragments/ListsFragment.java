package com.adelingo.android.swapi.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adelingo.android.swapi.R;
import com.adelingo.android.swapi.mvp.ListsFragmentPresenter;
import com.adelingo.android.swapi.mvp.ListsFragmentView;
import com.adelingo.android.swapi.ui.dialogs.InfoDialog;
import com.adelingo.android.swapi.ui.recycler.RecyclerViewAdapter;
import com.adelingo.android.swapi.utils.ItemClickSupport;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.swapi.models.Model;

// Фрагмент отвечает за отображение горизонтальных списков с информацией взятой из HTTP://SWAPI.CO/API/
// Списки реализованы с использованием RecyclerView
// Всего имеется 6 списков (Персонажи, Планеты, Фильмы, Рассы, Транспортные средства, Космические корабли)
// При Тап'е по элементу списка отображается InfoDialog c подробной информацией об этом элементе
// Восстанавливает актуальное состояние списков и лога при пересоздании Activity (поворот экрана, входящий вызов и т.д.)
// Память не "утекает"
public class ListsFragment extends Fragment implements ListsFragmentView {
    // Списки
    @Bind(R.id.list1)
    RecyclerView list1;
    @Bind(R.id.list2)
    RecyclerView list2;
    @Bind(R.id.list3)
    RecyclerView list3;
    @Bind(R.id.list4)
    RecyclerView list4;
    @Bind(R.id.list5)
    RecyclerView list5;
    @Bind(R.id.list6)
    RecyclerView list6;
    // Логи
    @Bind(R.id.textView)
    TextView tvLog;

    @Override
    public void setLog(String log) {
        tvLog.setText(log);
    }

    @Override
    public void setListAdapters(RecyclerViewAdapter a1, RecyclerViewAdapter a2, RecyclerViewAdapter a3, RecyclerViewAdapter a4, RecyclerViewAdapter a5, RecyclerViewAdapter a6) {
        list1.setAdapter(a1);
        list2.setAdapter(a2);
        list3.setAdapter(a3);
        list4.setAdapter(a4);
        list5.setAdapter(a5);
        list6.setAdapter(a6);
    }

    @Nullable
    @Override
    // Создаем View
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lists, container, false);
    }

    @Override
    @SuppressWarnings("unchecked")
    // Инициализируем элементы View
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setupViews();
        ListsFragmentPresenter.bind(this);
    }

    @Override
    // Освобождаем ресурсы (Вьюшки), чтобы предотвратить утечку памяти при пересоздании Activity
    public void onDestroyView() {
        ListsFragmentPresenter.unbind(this);
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    private void setupViews() {
        list1.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        list2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        list3.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        list4.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        list5.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        list6.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        ItemClickSupport.addTo(list1).setOnItemClickListener(listItemClickListener);
        ItemClickSupport.addTo(list2).setOnItemClickListener(listItemClickListener);
        ItemClickSupport.addTo(list3).setOnItemClickListener(listItemClickListener);
        ItemClickSupport.addTo(list4).setOnItemClickListener(listItemClickListener);
        ItemClickSupport.addTo(list5).setOnItemClickListener(listItemClickListener);
        ItemClickSupport.addTo(list6).setOnItemClickListener(listItemClickListener);
    }

    // Диалог для отображения полной информации о элементе списка. Открывается Тап'ом по элементу списка
    InfoDialog detailInfoDialog = new InfoDialog();

    private void openDetailInfoDialog(Model model) {
        if (model == null) return;
        Bundle bundle = new Bundle();
        bundle.putString(InfoDialog.ARGUMENT_TAG_TITLE, model.getName());
        bundle.putString(InfoDialog.ARGUMENT_TAG_CONTENT, model.getDescription());
        detailInfoDialog.setArguments(bundle);
        detailInfoDialog.show(this.getFragmentManager(), InfoDialog.class.getSimpleName());
    }

    ItemClickSupport.OnItemClickListener listItemClickListener = new ItemClickSupport.OnItemClickListener() {

        @Override
        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
            RecyclerViewAdapter adapter = (RecyclerViewAdapter) recyclerView.getAdapter();
            Model model = (Model) adapter.getList().get(position);
            openDetailInfoDialog(model);
        }
    };

}

package com.adelingo.android.swapi.events;

import co.swapi.models.Model;
import de.greenrobot.event.EventBus;

// Хелпер для отправки событий EventBus
public class EventHelper {
    private final static EventBus event = EventBus.getDefault();

    // Подписать на события
    public static void register(Object subscriber) {
        event.register(subscriber);
    }

    // Подписать на события и получить последнее прошедшее событие ("Липкая" подписка)
    public static void registerSticky(Object subscriber) {
        event.registerSticky(subscriber);
    }

    // Отписать от событий
    public static void unregister(Object subscriber) {
        event.unregister(subscriber);
    }

    // Событие. Начата загрузка
    public static void sendFetchStart() {
        event.removeStickyEvent(FetchCompleteEvent.class);
        event.removeStickyEvent(FetchErrorEvent.class);
        event.postSticky(new FetchStartEvent());
    }

    // Событие. Загрузка завершена успешно
    public static void sendFetchComplete() {
        event.removeStickyEvent(FetchStartEvent.class);
        event.postSticky(new FetchCompleteEvent());
    }

    // Событие. Загрузка завершена с ошибкой
    public static void sendFetchError(Throwable throwable) {
        event.removeStickyEvent(FetchStartEvent.class);
        event.postSticky(new FetchErrorEvent(throwable));
    }

    // Событие. Получены данные
    public static void sendModelFetched(Model model) {
        event.post(new ModelFetchedEvent(model));
    }
}

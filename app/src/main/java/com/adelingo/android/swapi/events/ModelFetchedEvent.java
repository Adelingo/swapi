package com.adelingo.android.swapi.events;

import co.swapi.models.Model;

//Событие. Получены данные
public class ModelFetchedEvent {
    public final Model model;

    ModelFetchedEvent(Model listModel) {
        this.model = listModel;
    }
}

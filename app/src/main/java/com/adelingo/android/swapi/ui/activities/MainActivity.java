package com.adelingo.android.swapi.ui.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.adelingo.android.swapi.R;
import com.adelingo.android.swapi.events.EventHelper;
import com.adelingo.android.swapi.events.FetchCompleteEvent;
import com.adelingo.android.swapi.events.FetchErrorEvent;
import com.adelingo.android.swapi.events.FetchStartEvent;
import com.adelingo.android.swapi.jobs.SwapiJob;
import com.adelingo.android.swapi.ui.fragments.ListsFragment;
import com.adelingo.android.swapi.ui.fragments.ProgressFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

// Activity содержит Toolbar, Fab, два фрагмента (один для прогресса, второй для данных) и кнопку для загрузки данных
// Также имеется CoordinatorLayout и картинка (постер)
public class MainActivity extends AppCompatActivity {

    @Bind(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @Bind(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @Bind(R.id.buttons_layout)
    ViewGroup buttonsLayout;
    @Bind(R.id.button1)
    Button button1;
    @Bind(R.id.fab)
    FloatingActionButton fab;

    int buttonsLayoutVisible = View.VISIBLE;

    // Принимает сообщения от EventBus
    EventsReceiver eventsReceiver = new EventsReceiver();

    class EventsReceiver {
        public void onEventMainThread(FetchStartEvent event) {
            hideButton();
        }

        public void onEventMainThread(FetchCompleteEvent event) {
            showButton();
        }

        public void onEventMainThread(FetchErrorEvent event) {
            showButton();
        }

        private void hideButton() {
            buttonsLayoutVisible = View.GONE;
            if (buttonsLayout != null) {
                buttonsLayout.setVisibility(buttonsLayoutVisible);
            }
        }

        private void showButton() {
            buttonsLayoutVisible = View.VISIBLE;
            if (buttonsLayout != null) {
                buttonsLayout.setVisibility(buttonsLayoutVisible);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        EventHelper.registerSticky(eventsReceiver);

        setSupportActionBar(toolbar);

        // При нажатии на кнопку Button1 инициируется загрузка данных с использованием Retrofit + RxJava + EventBus
        // Работа по загрузке данных выполняется в отдельных потоках (не более 4-х)
        // В процессе работы генерируются события EventBus
        // События EventBus "ловятся" Фрагментами и обрабатываются в UI-потоке
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.appBarLayout.setExpanded(false, true);    // Закрываем спойлер
                SwapiJob.fetch();
            }
        });

        // При нажатии на Fab открывается подробная информация о Дарт Вейдере (статью в Википедии открываем через браузер)
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Пробуем открыть статью о Дарт Вейдере в Википедии
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_wikipedia_darth_vader)));
                    startActivity(intent);
                }
                // Если на устройстве нет ни одного браузера, информируем об этом пользователя
                catch (ActivityNotFoundException e) {
                    Snackbar.make(view, R.string.browser_not_found, Snackbar.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });

        initFragments(savedInstanceState);
    }

    ProgressFragment progressFragment;  // Фрагмент с индикатором загрузки
    ListsFragment listsFragment;        // Фрагмент с горизонтальными списками

    // Т.к. в Фрагментах свойству Retain присваиваем значение true (не уничтожать при пересоздании Activity)
    // то необходимо делать проверку на их существование в FragmentManager
    // Фрагменты необходимо создавать только если ранее они не создавались
    private void initFragments(Bundle savedInstanceState) {
        final FragmentManager fragmentManager = getSupportFragmentManager();

        if (progressFragment == null)
            progressFragment = (ProgressFragment) fragmentManager.findFragmentByTag(ProgressFragment.class.getSimpleName());
        if (progressFragment == null) {
            progressFragment = new ProgressFragment();
        }
        if (listsFragment == null)
            listsFragment = (ListsFragment) fragmentManager.findFragmentByTag(ListsFragment.class.getSimpleName());
        if (listsFragment == null) {
            listsFragment = new ListsFragment();
        }

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_progress_container, progressFragment, progressFragment.getClass().getSimpleName());
        fragmentTransaction.replace(R.id.fragment_main_container, listsFragment, listsFragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    // Отписываемся от EventBus
    protected void onDestroy() {
        super.onDestroy();
        EventHelper.unregister(eventsReceiver);
    }
}

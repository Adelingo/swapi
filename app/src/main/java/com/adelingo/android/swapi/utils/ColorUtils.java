package com.adelingo.android.swapi.utils;

import android.content.res.TypedArray;

import com.adelingo.android.swapi.App;
import com.adelingo.android.swapi.R;

public class ColorUtils {
    public static String[] colorNames = App.getInstance().getResources().getStringArray(R.array.colorNames);
    public static TypedArray colors = App.getInstance().getResources().obtainTypedArray(R.array.colors);
    // Возвращает ресурс цвета по имени. Если соответствие не найдено, то возвращает -1
    public static int getColorResourceByName(String name) {
        int result = -1;
        if (name != null) {
            for (int i = 0; i < colorNames.length; i++) {
                if (name.equalsIgnoreCase(colorNames[i])) {
                    result = colors.getResourceId(i, 0);
                    break;
                }
            }
        }
        return result;
    }
}

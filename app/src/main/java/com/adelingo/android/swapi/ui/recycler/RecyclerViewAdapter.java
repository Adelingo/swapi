package com.adelingo.android.swapi.ui.recycler;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adelingo.android.swapi.R;
import com.adelingo.android.swapi.ui.fragments.ListsFragment;
import com.adelingo.android.swapi.utils.ColorUtils;

import java.util.ArrayList;
import java.util.List;

import co.swapi.models.Model;

// Адаптер для списков
// В зависимости от типа списка, список наполняется соответствующими данными
public class RecyclerViewAdapter<T extends Model> extends RecyclerView.Adapter<RecyclerViewHolder> {
    private final List<T> list;

    public RecyclerViewAdapter() {
        list = new ArrayList<>();
    }

    public void add(T item) {
        if (item != null) {
            list.add(item);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    public List<T> getList() {
        return this.list;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        final RecyclerViewHolder holder = new RecyclerViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Model item = list.get(position);
        int colorRes = ColorUtils.getColorResourceByName(item.getColor());
        if (colorRes < 0) colorRes = R.color.cardview_light_background;

        holder.text.setText(item.getName());
        holder.card.setCardBackgroundColor(ContextCompat.getColor(holder.card.getContext(), colorRes));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

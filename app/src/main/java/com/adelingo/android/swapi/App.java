package com.adelingo.android.swapi;

import android.app.Application;
import android.content.res.Resources;

import com.squareup.leakcanary.LeakCanary;

import timber.log.Timber;

/**
 * (орфография и пунктуация сохранены)
 * Задание: Реализовать приложение-справочник по вселенной "Звездные воины"
 * <p/>
 * Сделать одноэкранное приложение в которого будут входить горизонтальные
 * списки взятые из http://swapi.co/api/. Необходимо данные по каждой рубрике вынести в
 * отдельный список
 * <p/>
 * ----------------------  список_1
 * ----------------------  список_2
 * ......................  ...
 * ----------------------  список_N
 * <p/>
 * В каждый итем списка необходимо выводить его имя или название а фоном итема цвет.
 * <p/>
 * По клику на итем выводит диалоговое окно с подробной информацией(В нее входит
 * основная информация итема по каждой рубрике)
 * <p/>
 * Требования:
 * 1) Реализовать Toolbar(Предусмотреть тень на Android 4 и 5)
 * 2) Каждый итем списка должен быть CardView(предусмотреть тень на Android 4 и 5)
 * 3) Главный экран должен скролится вертикально
 * 4) Фон каждого итема брать из JSON если такого нету брать стандартный CardView
 * 5) Реализовать ProgressBar во весь экран при подтягивании данных
 * 6) Минимальная версия Android 4.1
 * 7) Реализовать FAB по клику на нее выводить подробную информацию о Дарт Вейдере
 * 8) Высоту списка отрегулировать так будто на фоне постер
 */

public class App extends Application {

    private static App sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        Timber.plant(new Timber.DebugTree());
        LeakCanary.install(this);
    }

    public static App getInstance() {
        return sInstance;
    }

    public static String getStringFromResource(int id) {
        try {
            return sInstance.getResources().getString(id);
        } catch (Resources.NotFoundException e) {
            Timber.e(e, Integer.toString(id));
        }
        return null;
    }

}

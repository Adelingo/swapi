package com.adelingo.android.swapi.mvp;

import com.adelingo.android.swapi.events.EventHelper;
import com.adelingo.android.swapi.events.FetchCompleteEvent;
import com.adelingo.android.swapi.events.FetchErrorEvent;
import com.adelingo.android.swapi.events.FetchStartEvent;

public class ProgressFragmentModel {

    public interface OnModelChanged {
        void onStart();

        void onComplete();

        void onError();
    }

    private final OnModelChanged presenter;

    public ProgressFragmentModel(OnModelChanged presenter) {
        if (presenter == null) throw new IllegalArgumentException();
        this.presenter = presenter;
        EventHelper.registerSticky(this);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(FetchStartEvent event) {
        presenter.onStart();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(FetchCompleteEvent event) {
        presenter.onComplete();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(FetchErrorEvent event) {
        presenter.onError();
    }

}

package com.adelingo.android.swapi.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.adelingo.android.swapi.R;
import com.adelingo.android.swapi.mvp.ProgressFragmentPresenter;
import com.adelingo.android.swapi.mvp.ProgressFragmentView;

import butterknife.Bind;
import butterknife.ButterKnife;

// Фрагмент отвечает за отображение ProgressBar во время загрузки данных по сети (работа с сетью выполняется в отдельных потоках)
// Восстанавливает актуальное состояние ProgressBar при пересоздании Activity (поворот экрана, входящий вызов и т.д.)
// Память не "утекает"
public class ProgressFragment extends Fragment implements ProgressFragmentView {

    @Bind(R.id.progressBar)
    ProgressBar mProgressBar;

    @Override
    // Создаем View
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_progress, container, false);
    }

    @Override
    // Инициализируем элементы View и подключаемся к Presenter
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        ProgressFragmentPresenter.bind(this);
    }

    @Override
    // Отключаемся от Presenter
    public void onDestroyView() {
        ProgressFragmentPresenter.unbind(this);
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    // Отобразить прогресс
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    // Скрыть прогресс
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

}

package com.adelingo.android.swapi.ui.recycler;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.adelingo.android.swapi.R;

import butterknife.Bind;
import butterknife.ButterKnife;

// Holder для Вьющек списка
public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.card)
    CardView card = null;
    @Bind(R.id.textView)
    TextView text = null;

    public RecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

package com.adelingo.android.swapi.events;

// Событие. Зашрузка завершилась с ошибкой
public class FetchErrorEvent {
    public final Throwable throwable;

    FetchErrorEvent(Throwable throwable) {
        this.throwable = throwable;
    }
}

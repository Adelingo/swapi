package com.adelingo.android.swapi.mvp;

import com.adelingo.android.swapi.events.EventHelper;
import com.adelingo.android.swapi.events.FetchCompleteEvent;
import com.adelingo.android.swapi.events.FetchErrorEvent;
import com.adelingo.android.swapi.events.FetchStartEvent;
import com.adelingo.android.swapi.events.ModelFetchedEvent;

import co.swapi.models.Model;

public class ListsFragmentModel {

    public interface OnModelChanged {
        void onStart();

        void onComplete();

        void onError(Throwable throwable);

        void onModel(Model model);
    }

    private final OnModelChanged presenter;

    public ListsFragmentModel(OnModelChanged presenter) {
        if (presenter == null) throw new IllegalArgumentException();
        this.presenter = presenter;
        EventHelper.registerSticky(this);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(FetchStartEvent event) {
        presenter.onStart();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(FetchCompleteEvent event) {
        presenter.onComplete();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(FetchErrorEvent event) {
        presenter.onError(event.throwable);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ModelFetchedEvent event) {
        presenter.onModel(event.model);
    }


}

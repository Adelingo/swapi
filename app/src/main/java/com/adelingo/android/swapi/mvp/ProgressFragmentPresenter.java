package com.adelingo.android.swapi.mvp;

public class ProgressFragmentPresenter implements ProgressFragmentModel.OnModelChanged {

    private static final ProgressFragmentPresenter singleton = new ProgressFragmentPresenter();

    private boolean progress;
    private ProgressFragmentView view;
    private ProgressFragmentModel model;

    private ProgressFragmentPresenter() {
        model = new ProgressFragmentModel(this);
    }

    @Override
    public void onStart() {
        setProgress(true);
    }

    @Override
    public void onComplete() {
        setProgress(false);
    }

    @Override
    public void onError() {
        setProgress(false);
    }

    private void setProgress(boolean progress) {
        this.progress = progress;
        updateView();
    }

    private void setView(ProgressFragmentView view) {
        this.view = view;
        updateView();
    }

    private void updateView() {
        if (view == null) return;
        if (progress) {
            view.showProgress();
        } else {
            view.hideProgress();
        }
    }

    public static void bind(ProgressFragmentView view) {
        if (singleton.view == null) {
            singleton.setView(view);
        } else {
            throw new IllegalStateException();
        }
    }

    public static void unbind(ProgressFragmentView view) {
        if (singleton.view == view) {
            singleton.setView(null);
        } else {
            throw new IllegalStateException();
        }
    }

}

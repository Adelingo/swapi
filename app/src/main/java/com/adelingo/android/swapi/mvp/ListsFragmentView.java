package com.adelingo.android.swapi.mvp;

import com.adelingo.android.swapi.ui.recycler.RecyclerViewAdapter;

public interface ListsFragmentView {
    void setLog(String message);

    void setListAdapters(RecyclerViewAdapter a1, RecyclerViewAdapter a2, RecyclerViewAdapter a3, RecyclerViewAdapter a4, RecyclerViewAdapter a5, RecyclerViewAdapter a6);
}

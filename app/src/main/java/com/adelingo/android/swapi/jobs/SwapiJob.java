package com.adelingo.android.swapi.jobs;

import com.adelingo.android.swapi.events.EventHelper;

import co.swapi.models.Model;
import co.swapi.service.Swapi;
import rx.Observer;
import rx.functions.Action0;

public class SwapiJob {
    private SwapiJob() {

    }

    public static void fetch() {
        Swapi.fetchRx()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        EventHelper.sendFetchStart();
                    }
                })
                .subscribe(new Observer<Model>() {                  // Запускаем загрузку данных
                    @Override
                    public void onCompleted() {
                        EventHelper.sendFetchComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        EventHelper.sendFetchError(e);
                    }

                    @Override
                    public void onNext(Model model) {
                        EventHelper.sendModelFetched(model);
                    }
                });
    }
}

package com.adelingo.android.swapi.mvp;

public interface ProgressFragmentView {
    void showProgress();

    void hideProgress();
}

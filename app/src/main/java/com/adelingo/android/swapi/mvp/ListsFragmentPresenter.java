package com.adelingo.android.swapi.mvp;

import com.adelingo.android.swapi.App;
import com.adelingo.android.swapi.R;
import com.adelingo.android.swapi.ui.recycler.RecyclerViewAdapter;

import co.swapi.models.Film;
import co.swapi.models.Model;
import co.swapi.models.People;
import co.swapi.models.Planet;
import co.swapi.models.Specie;
import co.swapi.models.Starship;
import co.swapi.models.Vehicle;

public class ListsFragmentPresenter implements ListsFragmentModel.OnModelChanged {

    private static final ListsFragmentPresenter singleton = new ListsFragmentPresenter();

    private ListsFragmentView view;
    private ListsFragmentModel model;

    private StringBuffer logText = new StringBuffer();
    private final RecyclerViewAdapter<Film> filmsAdapter = new RecyclerViewAdapter<>();
    private final RecyclerViewAdapter<People> peoplesAdapter = new RecyclerViewAdapter<>();
    private final RecyclerViewAdapter<Planet> planetsAdapter = new RecyclerViewAdapter<>();
    private final RecyclerViewAdapter<Specie> speciesAdapter = new RecyclerViewAdapter<>();
    private final RecyclerViewAdapter<Starship> starshipsAdapter = new RecyclerViewAdapter<>();
    private final RecyclerViewAdapter<Vehicle> vehiclesAdapter = new RecyclerViewAdapter<>();

    private ListsFragmentPresenter() {
        model = new ListsFragmentModel(this);
    }

    private void clearAdapters() {
        filmsAdapter.clear();
        peoplesAdapter.clear();
        planetsAdapter.clear();
        speciesAdapter.clear();
        starshipsAdapter.clear();
        vehiclesAdapter.clear();
        updateAdapters();
    }

    private void clearLog() {
        logText.setLength(0);
        updateLog();
    }


    private void add(Model model) {
        if (model instanceof Film) {
            filmsAdapter.add((Film) model);
        } else if (model instanceof People) {
            peoplesAdapter.add((People) model);
        } else if (model instanceof Planet) {
            planetsAdapter.add((Planet) model);
        } else if (model instanceof Specie) {
            speciesAdapter.add((Specie) model);
        } else if (model instanceof Starship) {
            starshipsAdapter.add((Starship) model);
        } else if (model instanceof Vehicle) {
            vehiclesAdapter.add((Vehicle) model);
        } else {
            throw new IllegalArgumentException(model.toString());
        }
    }

    @Override
    public void onStart() {
        clearAdapters();
        clearLog();
        appendLog(App.getStringFromResource(R.string.used_patterns_and_libraries));
        appendLog();
        appendLog(App.getStringFromResource(R.string.start));
    }

    @Override
    public void onComplete() {
        appendLog(App.getStringFromResource(R.string.complete));
    }

    @Override
    public void onError(Throwable throwable) {
        appendLog(App.getStringFromResource(R.string.error), throwable.getMessage());
    }

    @Override
    public void onModel(Model model) {
        appendLog("\t", model.getClass().getSimpleName(), "->", model.getName());
        add(model);
    }

    private void setView(ListsFragmentView view) {
        if (view == null && this.view != null) {
            this.view.setListAdapters(null, null, null, null, null, null);
        }
        this.view = view;
        updateAdapters();
        updateLog();
    }

    private void updateAdapters() {
        if (view != null) {
            view.setListAdapters(
                    filmsAdapter,
                    peoplesAdapter,
                    planetsAdapter,
                    speciesAdapter,
                    starshipsAdapter,
                    vehiclesAdapter);
        }
    }

    private void updateLog() {
        if (view != null) {
            view.setLog(logText.toString());
        }
    }

    private void appendLog(String... messages) {
        for (String message : messages) {
            logText.append(message);
        }
        logText.append("\n");
        updateLog();
    }

    public static void bind(ListsFragmentView view) {
        singleton.appendLog(App.getStringFromResource(R.string.bind));
        if (singleton.view == null) {
            singleton.setView(view);
        } else {
            throw new IllegalStateException();
        }
    }

    public static void unbind(ListsFragmentView view) {
        singleton.appendLog(App.getStringFromResource(R.string.unbind));
        if (singleton.view == view) {
            singleton.setView(null);
        } else {
            throw new IllegalStateException();
        }
    }

}

package co.swapi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.swapi.models.Film;
import co.swapi.models.Model;
import co.swapi.models.Page;
import co.swapi.models.People;
import co.swapi.models.Planet;
import co.swapi.models.Specie;
import co.swapi.models.Starship;
import co.swapi.models.Vehicle;
import co.swapi.service.Swapi;
import rx.Subscriber;

public class MainClass {
    public static void main(String[] args) throws IOException {
/*        System.out.println("--- Sync ---");

        Film film = Swapi.fetch(Film.class, 1);
        People people = Swapi.fetch(People.class, 1);
        Planet planet = Swapi.fetch(Planet.class, 1);
        Specie specie = Swapi.fetch(Specie.class, 1);
        Starship starship = Swapi.fetch(Starship.class, 2);
        Vehicle vehicle = Swapi.fetch(Vehicle.class, 20);

        Page<Film> films = Swapi.fetchPage(Film.class, null);
        Page<People> peoples = Swapi.fetchPage(People.class, null);
        Page<Planet> planets = Swapi.fetchPage(Planet.class, null);
        Page<Specie> species = Swapi.fetchPage(Specie.class, null);
        Page<Starship> starships = Swapi.fetchPage(Starship.class, null);
        Page<Vehicle> vehicles = Swapi.fetchPage(Vehicle.class, null);

        List<Film> filmList = Swapi.fetch(Film.class);
        List<People> peopleList = Swapi.fetch(People.class);
        List<Planet> planetList = Swapi.fetch(Planet.class);
        List<Specie> specieList = Swapi.fetch(Specie.class);
        List<Starship> starshipList = Swapi.fetch(Starship.class);
        List<Vehicle> vehicleList = Swapi.fetch(Vehicle.class);
*/
        System.out.println("--- Rx Async ---");
        final List<Model> rxList = new ArrayList<>();
        Swapi.fetchRx()
                .subscribe(new Subscriber<Model>() {
                    @Override
                    public void onCompleted() {
                        System.out.println("onCompleted");

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Model model) {
                        rxList.add(model);
                        System.out.println(Thread.currentThread().getName() + " " +model.getClass().getSimpleName() + ": " + model.getName());
                    }
                });

        System.out.println("--- Rx Async Sheduled ---");

        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Swapi.fetchPageRx(People.class).subscribe();
        System.out.println("--- Finish ---");

    }
}

package co.swapi.service;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import co.swapi.models.Film;
import co.swapi.models.Model;
import co.swapi.models.Page;
import co.swapi.models.People;
import co.swapi.models.Planet;
import co.swapi.models.Root;
import co.swapi.models.Specie;
import co.swapi.models.Starship;
import co.swapi.models.Vehicle;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Swapi {

    private Swapi() {

    }

    // Сервис Retrofit'а
    private static final String BASE_URL = "http://swapi.co/api/";
    private static final SwapiService SERVICE = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create(getGson()))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build()
            .create(SwapiService.class);

    // Настраиваем парсер Gson
    private static Gson getGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                        //.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX")   // для Core Java
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")      // для Android
                .create();
    }

    // Настраиваем Http-Клиент
    private static OkHttpClient getOkHttpClient() {
        OkHttpClient client = new OkHttpClient();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        client.interceptors().add(logging);
        return client;
    }


//----------------------------------------------------------------------------
//------Реализация запросов Retrofit с использованием RxJava------
//----------------------------------------------------------------------------

    // Ограничиваем максимальное количество одновременных сетевых запросов - не более 4-х
    private static final Executor networkExecutor = Executors.newScheduledThreadPool(4);
    // Все результаты сетевых запросов будем обрабатывать в одном отдельном потоке
    private static final Executor observerExecutor = Executors.newSingleThreadExecutor();

    // Обертка для распараллеливания сетевых запросов
    private static <T extends Object> Observable<T> asyncNetwork(Observable<T> observable) {
        return observable
                .subscribeOn(Schedulers.from(networkExecutor))  // не более 4 одновременных сетевых запросов
                .observeOn(Schedulers.from(observerExecutor));  // все результаты обрабатываются в одном отдельном потоке
    }

    // Загружает <Root>
    public static Observable<String> fetchRootRx() {
        return asyncNetwork(SERVICE.getRootRx())
                .flatMap(new Func1<Root, Observable<String>>() {
                    @Override
                    public Observable<String> call(Root root) {
                        String[] urls = {root.films, root.people, root.planets, root.species, root.starships, root.vehicles};
                        return Observable.from(urls);
                    }
                });
    }

    // Загружает <Model> типа clazz с идентификатором id
    @SuppressWarnings("unchecked")
    public static <T extends Model> Observable<T> fetchRx(Class<T> clazz, Integer id) {
        if (id == null) id = 0;
        Observable<?> observable;
        if (Film.class.equals(clazz)) {
            observable = SERVICE.getFilmRx(id);
        } else if (People.class.equals(clazz)) {
            observable = SERVICE.getPeopleRx(id);
        } else if (Planet.class.equals(clazz)) {
            observable = SERVICE.getPlanetRx(id);
        } else if (Specie.class.equals(clazz)) {
            observable = SERVICE.getSpecieRx(id);
        } else if (Starship.class.equals(clazz)) {
            observable = SERVICE.getStarshipRx(id);
        } else if (Vehicle.class.equals(clazz)) {
            observable = SERVICE.getVehicleRx(id);
        } else {
            throw new IllegalArgumentException(clazz.toString());
        }
        return (Observable<T>) asyncNetwork(observable);
    }

    // загружает <Page> с номером page для clazz
    @SuppressWarnings("unchecked")
    public static <T extends Model> Observable<Page<T>> fetchPageRx(Class<T> clazz, Integer page) {
        Observable<?> observable;
        if (Film.class.equals(clazz)) {
            observable = SERVICE.getFilmsRx(page);
        } else if (People.class.equals(clazz)) {
            observable = SERVICE.getPeoplesRx(page);
        } else if (Planet.class.equals(clazz)) {
            observable = SERVICE.getPlanetsRx(page);
        } else if (Specie.class.equals(clazz)) {
            observable = SERVICE.getSpeciesRx(page);
        } else if (Starship.class.equals(clazz)) {
            observable = SERVICE.getStarshipsRx(page);
        } else if (Vehicle.class.equals(clazz)) {
            observable = SERVICE.getVehiclesRx(page);
        } else {
            throw new IllegalArgumentException(clazz.toString());
        }
        return (Observable<Page<T>>) asyncNetwork(observable);
    }

    // Загружает все <Page> типа clazz
    public static <T extends Model> Observable<Page<T>> fetchPageRx(Class<T> clazz) {
        return getPageAndNext(clazz, null);
    }

    // Загружает абсолютно все данные
    @SuppressWarnings("unchecked")
    public static <T extends Model> Observable<T> fetchRx() {
        return fetchRootRx()
                .flatMap(new Func1<String, Observable<Page<T>>>() {
                    @Override
                    public Observable<Page<T>> call(String s) {
                        Observable<?> observable;
                        if (s.equalsIgnoreCase(BASE_URL + "films/"))
                            observable = fetchPageRx(Film.class);
                        else if (s.equalsIgnoreCase(BASE_URL + "people/"))
                            observable = fetchPageRx(People.class);
                        else if (s.equalsIgnoreCase(BASE_URL + "planets/"))
                            observable = fetchPageRx(Planet.class);
                        else if (s.equalsIgnoreCase(BASE_URL + "species/"))
                            observable = fetchPageRx(Specie.class);
                        else if (s.equalsIgnoreCase(BASE_URL + "starships/"))
                            observable = fetchPageRx(Starship.class);
                        else if (s.equalsIgnoreCase(BASE_URL + "vehicles/"))
                            observable = fetchPageRx(Vehicle.class);
                        else throw new IllegalArgumentException(s);
                        return (Observable<Page<T>>) observable;
                    }
                })
                .flatMap(new Func1<Page<? extends Model>, Observable<T>>() {
                    @Override
                    public Observable<T> call(Page<? extends Model> page) {
                        Observable<?> observable = Observable.from(page.getResults());
                        return (Observable<T>) observable;
                    }
                });
    }

    // Рекурсивная загрузка всех <Page> для clazz, начиная со страницы page
    private static <T extends Model> Observable<Page<T>> getPageAndNext(final Class<T> clazz, Integer page) {
        return fetchPageRx(clazz, page)
                .concatMap(new Func1<Page<T>, Observable<Page<T>>>() {
                    @Override
                    public Observable<Page<T>> call(Page<T> response) {
                        if (response.getNextPage() > 0) {
                            return Observable.just(response)
                                    .concatWith(getPageAndNext(clazz, response.getNextPage()));
                        } else {
                            return Observable.just(response);
                        }
                    }
                });
    }

//----------------------------------------------------------------------------------
//----Для полноты библиотеки реализуем запросы Retrofit без использования RxJava----
//----------------------------------------------------------------------------------

    // Загружает <Root>
    public static Root fetchRoot() throws IOException {
        return SERVICE.getRoot().execute().body();
    }

    // Загружает одну <Model> типа clazz с идентификатором id
    @SuppressWarnings("unchecked")
    public static <T extends Model> T fetch(Class<T> clazz, Integer id) throws IOException {
        Call<?> call;
        if (Film.class.equals(clazz)) {
            call = SERVICE.getFilm(id);
        } else if (People.class.equals(clazz)) {
            call = SERVICE.getPeople(id);
        } else if (Planet.class.equals(clazz)) {
            call = SERVICE.getPlanet(id);
        } else if (Specie.class.equals(clazz)) {
            call = SERVICE.getSpecie(id);
        } else if (Starship.class.equals(clazz)) {
            call = SERVICE.getStarship(id);
        } else if (Vehicle.class.equals(clazz)) {
            call = SERVICE.getVehicle(id);
        } else {
            throw new IllegalArgumentException(clazz.toString());
        }
        return (T) call.execute().body();
    }

    // Загружает одну <Page> с номером page типа clazz
    @SuppressWarnings("unchecked")
    public static <T extends Model> Page<T> fetchPage(Class<T> clazz, Integer page) throws IOException {
        Call<?> call;
        if (Film.class.equals(clazz)) {
            call = SERVICE.getFilms(page);
        } else if (People.class.equals(clazz)) {
            call = SERVICE.getPeoples(page);
        } else if (Planet.class.equals(clazz)) {
            call = SERVICE.getPlanets(page);
        } else if (Specie.class.equals(clazz)) {
            call = SERVICE.getSpecies(page);
        } else if (Starship.class.equals(clazz)) {
            call = SERVICE.getStarships(page);
        } else if (Vehicle.class.equals(clazz)) {
            call = SERVICE.getVehicles(page);
        } else {
            throw new IllegalArgumentException(clazz.toString());
        }
        return (Page<T>) call.execute().body();
    }

    // Загружает все <Model> типа clazz
    public static <T extends Model> List<T> fetch(Class<T> clazz) throws IOException {
        List<T> result = new ArrayList<>();
        Integer page = null;
        do {
            Page<T> data = fetchPage(clazz, page);
            result.addAll(data.getResults());
            page = data.getNextPage();
        } while (page > 0);
        return result;
    }
}
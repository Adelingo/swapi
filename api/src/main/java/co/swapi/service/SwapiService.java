package co.swapi.service;

import co.swapi.models.Film;
import co.swapi.models.Page;
import co.swapi.models.People;
import co.swapi.models.Planet;
import co.swapi.models.Root;
import co.swapi.models.Specie;
import co.swapi.models.Starship;
import co.swapi.models.Vehicle;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

interface SwapiService {

    @GET("/api")
    Call<Root> getRoot();

    @GET("films")
    Call<Page<Film>> getFilms(@Query("page") Integer page);

    @GET("people")
    Call<Page<People>> getPeoples(@Query("page") Integer page);

    @GET("planets")
    Call<Page<Planet>> getPlanets(@Query("page") Integer page);

    @GET("species")
    Call<Page<Specie>> getSpecies(@Query("page") Integer page);

    @GET("starships")
    Call<Page<Starship>> getStarships(@Query("page") Integer page);

    @GET("vehicles")
    Call<Page<Vehicle>> getVehicles(@Query("page") Integer page);

    @GET("films/{id}")
    Call<Film> getFilm(@Path("id") Integer id);

    @GET("people/{id}")
    Call<People> getPeople(@Path("id") Integer id);

    @GET("planets/{id}")
    Call<Planet> getPlanet(@Path("id") Integer id);

    @GET("species/{id}")
    Call<Specie> getSpecie(@Path("id") Integer id);

    @GET("starships/{id}")
    Call<Starship> getStarship(@Path("id") Integer id);

    @GET("vehicles/{id}")
    Call<Vehicle> getVehicle(@Path("id") Integer id);

    //-------------RxJava Support--------------------
    @GET("/api")
    Observable<Root> getRootRx();

    @GET("films")
    Observable<Page<Film>> getFilmsRx(@Query("page") Integer page);

    @GET("people")
    Observable<Page<People>> getPeoplesRx(@Query("page") Integer page);

    @GET("planets")
    Observable<Page<Planet>> getPlanetsRx(@Query("page") Integer page);

    @GET("species")
    Observable<Page<Specie>> getSpeciesRx(@Query("page") Integer page);

    @GET("starships")
    Observable<Page<Starship>> getStarshipsRx(@Query("page") Integer page);

    @GET("vehicles")
    Observable<Page<Vehicle>> getVehiclesRx(@Query("page") Integer page);

    @GET("films/{id}")
    Observable<Film> getFilmRx(@Path("id") Integer id);

    @GET("people/{id}")
    Observable<People> getPeopleRx(@Path("id") Integer id);

    @GET("planets/{id}")
    Observable<Planet> getPlanetRx(@Path("id") Integer id);

    @GET("species/{id}")
    Observable<Specie> getSpecieRx(@Path("id") Integer id);

    @GET("starships/{id}")
    Observable<Starship> getStarshipRx(@Path("id") Integer id);

    @GET("vehicles/{id}")
    Observable<Vehicle> getVehicleRx(@Path("id") Integer id);
}

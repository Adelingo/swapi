package co.swapi.models;

import java.util.Date;

// Планета
public class Planet extends Model {
    public String name;
    public String rotationPeriod;
    public String orbitalPeriod;
    public String diameter;
    public String climate;
    public String gravity;
    public String terrain;
    public String surfaceWater;
    public String population;
    public String[] residents;
    public String[] films;
    public Date created;
    public Date edited;
    public String url;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: " + name + "\n");
        sb.append("diameter: " + diameter + "\n");
        sb.append("rotation period: " + rotationPeriod + "\n");
        sb.append("orbital period: " + orbitalPeriod + "\n");
        sb.append("gravity: " + gravity + "\n");
        sb.append("population: " + population + "\n");
        sb.append("climate: " + climate + "\n");
        sb.append("terrain: " + terrain + "\n");
        sb.append("surfaceWater: " + surfaceWater + "\n");
        sb.append("residents count: " + residents.length + "\n");
        sb.append("films count: " + films.length + "\n");
        return sb.toString();
    }
}

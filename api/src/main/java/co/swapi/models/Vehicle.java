package co.swapi.models;

import java.util.Date;

// Транспортное средство
public class Vehicle extends Model {
    public String name;
    public String model;
    public String manufacturer;
    public String costInCredits;
    public String length;
    public String maxAtmospheringSpeed;
    public String crew;
    public String passengers;
    public String cargoCapacity;
    public String consumables;
    public String vehicleClass;
    public String[] pilots;
    public String[] films;
    public Date created;
    public Date edited;
    public String url;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: " + name + "\n");
        sb.append("Model: " + model + "\n");
        sb.append("vehicle class: " + vehicleClass + "\n");
        sb.append("manufacturer: " + manufacturer + "\n");
        sb.append("lenght: " + length + "\n");
        sb.append("cost in credits: " + costInCredits + "\n");
        sb.append("crew: " + crew + "\n");
        sb.append("passengers: " + passengers + "\n");
        sb.append("max atmospheric speed: " + maxAtmospheringSpeed + "\n");
        sb.append("cargo capacity: " + cargoCapacity + "\n");
        sb.append("consumables: " + consumables + "\n");
        sb.append("films count: " + films.length + "\n");
        sb.append("pilots count: " + pilots.length + "\n");
        return sb.toString();
    }
}

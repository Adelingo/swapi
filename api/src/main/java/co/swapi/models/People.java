package co.swapi.models;

import java.util.Date;

// Персонаж
public class People extends Model {
    public String name;
    public String height;
    public String mass;
    public String hairColor;
    public String skinColor;
    public String eyeColor;
    public String birthYear;
    public String gender;
    public String homeworld;
    public String[] films;
    public String[] species;
    public String[] vehicles;
    public String[] starships;
    public Date created;
    public Date edited;
    public String url;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: " + name + "\n");
        sb.append("birthYear: " + birthYear + "\n");
        sb.append("eyeColor: " + eyeColor + "\n");
        sb.append("gender: " + gender + "\n");
        sb.append("hairColor: " + hairColor + "\n");
        sb.append("height: " + height + "\n");
        sb.append("mass: " + mass + "\n");
        sb.append("skinColor: " + skinColor + "\n");
        sb.append("homeworld: " + homeworld + "\n");
        sb.append("films count: " + films.length + "\n");
        sb.append("species count: " + species.length + "\n");
        sb.append("starships count: " + starships.length + "\n");
        sb.append("vehicles count: " + vehicles.length + "\n");
        return sb.toString();
    }

    @Override
    public String getColor() {
        return eyeColor;
    }
}

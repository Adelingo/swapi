package co.swapi.models;

import java.util.Date;

//Фильм
public class Film extends Model {
    public String title;
    public int episodeId;
    public String openingCrawl;
    public String director;
    public String producer;
    public String releaseDate;
    public String[] characters;
    public String[] planets;
    public String[] starships;
    public String[] vehicles;
    public String[] species;
    public Date created;
    public Date edited;
    public String url;

    @Override
    public String getName() {
        return title;
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append("title: " + title + "\n");
        sb.append("episodeId: " + episodeId + "\n");
        sb.append("openingCrawl: " + openingCrawl + "\n");
        sb.append("director: " + director + "\n");
        sb.append("producer: " + producer + "\n");
        sb.append("releaseDate: " + releaseDate + "\n");
        sb.append("species count: " + species.length + "\n");
        sb.append("starships count: " + starships.length + "\n");
        sb.append("vehicles count: " + vehicles.length + "\n");
        sb.append("characters count: " + characters.length + "\n");
        sb.append("planets count: " + planets.length + "\n");
        return sb.toString();
    }
}

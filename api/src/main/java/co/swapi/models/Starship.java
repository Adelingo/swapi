package co.swapi.models;

import java.util.Date;

// Космический корабль
public class Starship extends Model {
    public String name;
    public String model;
    public String manufacturer;
    public String costInCredits;
    public String length;
    public String maxAtmospheringSpeed;
    public String crew;
    public String passengers;
    public String cargoCapacity;
    public String consumables;
    public String hyperdriveRating;
    public String MGLT;
    public String starshipClass;
    public String[] pilots;
    public String[] films;
    public Date created;
    public Date edited;
    public String url;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: " + name + "\n");
        sb.append("Model: " + model + "\n");
        sb.append("starship class: " + starshipClass + "\n");
        sb.append("manufacturer: " + manufacturer + "\n");
        sb.append("cost in credits: " + costInCredits + "\n");
        sb.append("length: " + length + "\n");
        sb.append("crew: " + crew + "\n");
        sb.append("passengers: " + passengers + "\n");
        sb.append("max atmosphering speed: " + maxAtmospheringSpeed + "\n");
        sb.append("hyperdrive rating: " + hyperdriveRating + "\n");
        sb.append("MGLT: " + MGLT + "\n");
        sb.append("cargo capacity: " + cargoCapacity + "\n");
        sb.append("consumables: " + consumables + "\n");
        sb.append("films count: " + films.length + "\n");
        sb.append("pilots count: " + pilots.length + "\n");
        return sb.toString();
    }
}

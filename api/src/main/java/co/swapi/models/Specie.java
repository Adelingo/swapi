package co.swapi.models;

import java.util.Date;

// Расса / Вид
public class Specie extends Model {
    public String name;
    public String classification;
    public String designation;
    public String averageHeight;
    public String skinColors;
    public String hairColors;
    public String eyeColors;
    public String averageLifespan;
    public String homeworld;
    public String language;
    public String[] people;
    public String[] films;
    public Date created;
    public Date edited;
    public String url;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: " + name + "\n");
        sb.append("classification: " + classification + "\n");
        sb.append("designation: " + designation + "\n");
        sb.append("average height: " + averageHeight + "\n");
        sb.append("average lifespan: " + averageLifespan + "\n");
        sb.append("eye colors: " + eyeColors + "\n");
        sb.append("hair colors: " + hairColors + "\n");
        sb.append("skin colors: " + skinColors + "\n");
        sb.append("language: " + language + "\n");
        sb.append("homeworld: " + homeworld + "\n");
        sb.append("people count: " + people.length + "\n");
        sb.append("films count: " + films.length + "\n");
        return sb.toString();
    }

    @Override
    public String getColor() {
        try {
            String[] colors = eyeColors.split(",");
            return colors[0];
        } catch (Exception e) {
        }
        return super.getColor();
    }
}

package co.swapi.models;

import java.util.List;

// Страница, содержащая данные по Model
public class Page<T extends Model> {
    int count;
    String next;
    String previous;
    List<T> results;

    private int cNext = -1;
    private int cPrevious = -1;

    public int getNextPage() {
        if (cNext == -1) {
            cNext = getPageFromUrl(next);
        }
        return cNext;
    }

    public int getPreviousPage() {
        if (cPrevious == -1) {
            cPrevious = getPageFromUrl(previous);
        }
        return cPrevious;
    }

    public List<T> getResults() {
        return results;
    }

    private static final String PARAM_PAGE = "?page=";

    private int getPageFromUrl(String url) {
        if (url == null) return 0;
        int index = url.toLowerCase().lastIndexOf(PARAM_PAGE);
        if (index != -1) {
            String page = url.substring(index + PARAM_PAGE.length());
            try {
                return Integer.valueOf(page);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }
}

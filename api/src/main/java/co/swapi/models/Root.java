package co.swapi.models;

// Корневой элемент API
public class Root {
    public String people;
    public String planets;
    public String films;
    public String species;
    public String vehicles;
    public String starships;
}
package co.swapi.interfaces;

public interface ModelDescription {
    String getName();

    String getDescription();

    String getColor();
}

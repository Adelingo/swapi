```
#!Java

/**
 * Задание: Реализовать приложение-справочник по вселенной "Звездные воины"
 * (орфография и пунктуация сохранены)
 *
 * Сделать одноэкранное приложение в которого будут входить горизонтальные
 * списки взятые из http://swapi.co/api/. Необходимо данные по каждой рубрике вынести в
 * отдельный список
 *
 *      ----------------------  список_1
 *      ----------------------  список_2
 *      ......................  ...
 *      ----------------------  список_N
 *
 * В каждый итем списка необходимо выводить его имя или название а фоном итема цвет.
 *
 * По клику на итем выводит диалоговое окно с подробной информацией(В нее входит
 * основная информация итема по каждой рубрике)
 *
 * Требования:
 * 1) Реализовать Toolbar(Предусмотреть тень на Android 4 и 5)
 * 2) Каждый итем списка должен быть CardView(предусмотреть тень на Android 4 и 5)
 * 3) Главный экран должен скролится вертикально
 * 4) Фон каждого итема брать из JSON если такого нету брать стандартный CardView
 * 5) Реализовать ProgressBar во весь экран при подтягивании данных
 * 6) Минимальная версия Android 4.1
 * 7) Реализовать FAB по клику на нее выводить подробную информацию о Дарт Вейдере
 * 8) Высоту списка отрегулировать так будто на фоне постер
 */

```

Результат: Material Design + ButterKnife + Retrofit + RxJava + EventBus + Timber + LeakCanary

![device-2015-12-31-201139.png](https://bitbucket.org/repo/g6nd8b/images/893325562-device-2015-12-31-201139.png)

![device-2015-12-31-201340.png](https://bitbucket.org/repo/g6nd8b/images/1241636396-device-2015-12-31-201340.png)